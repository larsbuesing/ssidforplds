# Welcome

This repository contains methods for nonlinear spectral learning for linear dynamical system models with generalised-linear observations. In addition, it contains several functions for calculating the means and covariances of Poisson models receiving correlated Gaussian inputs.

It has been developed and implemented with the goal of modelling spike-train recordings from neural populations, but at least some of the methods will be applicable more generally. 

A view different rate-nonlinearities are implemented, functions which use the exponential nonlinearity are most efficient.

## Usage

To get started, run the example script: ./Examples/SSIDforPLDSExample.m
or one of the other scripts in ./Examples


If you notice a bug, want to request a feature, or have a question or feedback, please make use of the issue-tracking capabilities of the repository. We love to hear from people using our code-- please send an email to info@mackelab.org.

The code is published under the GNU General Public License. The code is provided "as is" and has no warranty whatsoever. 

## Publications

The code is based on 

###  L Buesing*, JH Macke*, M Sahani: Spectral learning of linear dynamics from generalised-linear observations with application to neural population data, In Advances in Neural Information Processing Systems (NIPS) 25, 2012.

and also described in 

### JH Macke, L Buesing, M Sahani: Estimating state and model parameters in state-space models of spike trains. Book-chapter, in preparation.
