clear all
close all

%%%%%%%%%%%%%%%%%%%%%%%% generate artifical system and sample from it %%%%%%%%%%%%%%%%%%%%%%%%

Trials     = 1000;
T          = 1000;	% length of each time-series, i.e. trial
xDim       = 5;     	% dimensionality of latent input
yDim       = 25;    	% dimensionality of observable
Bdim       = 2;     	% dimensionality of stimulus innovations
algo       = 'N4SID'; 	% available algorithms 'SVD','CCA','N4SID'

[seq, trueparams] = GenerateArtificialPLDSdata(xDim,yDim,Trials,T,Bdim);


%%%%%%%%%%%%%%%%%%%%%%%%%% estimate parameters from data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[params,SIGBig] = FitPLDSParamsSSID(seq,xDim,'algo',algo,'useB',true);


%%%%%%%%%%%%%%%%%%%%%%%%%% some analysis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('-----------------')
disp('Some analysis:')
fprintf('\nSubspace angle between true and estimated parameters: %d \n\n',subspace(trueparams.C,params.C))

disp('True eigenspectrum:')
sort(eig(trueparams.A))
disp('Estimated spectrum')
sort(eig(params.A))

disp('Plot of true and estimated data covariance matrix')
figure()
hcov   = cov([seq.h]');
tpCov  = trueparams.C*trueparams.Q0*trueparams.C';
tpCov  = tpCov+trueparams.C*trueparams.B*hcov*trueparams.B'*trueparams.C';
estCov = params.C*params.Q0*params.C';
estCov = estCov + params.C*params.B*hcov*params.B'*params.C';
imagesc([tpCov estCov])


